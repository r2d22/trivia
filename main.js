console.log("bic banana nana ben");
const valorBarra = 10
let score = 0
let currentQuestion = 0
let contadorPregunta = 0
let preguntasTotales = 0
let nivelAvance = valorBarra
let numPreguntas = 0

function inicializarArray() {
  preguntasRespuestas = [
    {
      question: "¿Qué recogería ‘document.getElementsByClassName(‘boton’)’?",
      options: ["Un elemento con el class ‘boton’", "Un botón", "Un array"],
      answer: "Un array"
    },
    {
      question:
        "¿Para qué utilizarías la función slice en el array de tu roleta?",
      options: [
        "Para modificar el array original eliminando los valores que se le indica",
        "Para que devuelva los valores que le pido sin modificar el array original",
        "Para evitar que seamos elegidos para hacer la példora"
      ],
      answer: "Para que devuelva los valores que le pido sin modificar el array original"
    },
    {
      question:
        "¿Cuál es la principal aportación de Ada Lovelace a la programación?",
      options: [
        "La máquina analítica en colaboración con Charles Babbage",
        "El lenguaje de programación ‘Ada’",
        "Un algoritmo diseñado para ser leído por una máquina"
      ],
      answer: "Un algoritmo diseñado para ser leído por una máquina"
    },
    {
      question: "¿Una diferencia entre Flexbox y CSS Grid?",
      options: [
        "Flexbox es adecuado para alinear contenido específico ",
        "CSS Grid sirve para diseñar el layout general",
        "Ambas son correctas"
      ],
      answer: "Ambas son correctas"
    },
    {
      question: "¿Cuál es el código identificativo de este curso?",
      options: ["19480771", "adminadmin", "pf5PF5"],
      answer: "19480771"
    },
    {
      question: "¿Para qué tipo de tiendas online se usa principalmente Magento?",
      options: ["Pequeñas", "Grandes", "Medianas"],
      answer: "Grandes"
    },
    {
      question: "¿En qué lenguaje de programación están la mayoría de los cms?",
      options: ["Java", "Go", "PHP"],
      answer: "PHP"
    },
    {
      question: "Jon y Josu, ¿Quién es mayor?",
      options: ["Jon < Josu", "Jon > Josu", "Sabes quién es mayor, lo que no sabes es cómo se llama"],
      answer: "Jon < Josu"
    },
  
    {
      question: "¿Con qué objeto físico comparó Ziortza una variable?",
      options: ["Una memoria", "Una llave inglesa", "La caja, ¡LA CAJA!"],
      answer: "La caja, ¡LA CAJA!"
    },
    {
      question: "¿Quién es el César Venezolano?",
      options: ["César C.", "César O.", "Efectivamente"],
      answer: "César O."
    }
  ]
}

var botonAlJuego = document.getElementById("buttonToGame");
var botonNext = document.getElementById("buttonNext");
var botonCheck = document.getElementById("buttonCheck");
var botonReset = document.getElementById("buttonToRestart");

botonAlJuego.addEventListener("click", irAlJuego);
botonNext.addEventListener("click", preguntaSiguiente);
botonCheck.addEventListener("click", comprobarCorrecta);
botonReset.addEventListener("click", volverInicio);

function pantallasBloqueadas() {
  console.log("pantallasBloqueadas");
  document.getElementById("start").style.display = "none";
  document.getElementById("game").style.display = "none";
  document.getElementById("fin").style.display = "none";
}

function mostrarPantallaGame() {
  document.getElementById("game").style.display = "block";
}

function mostrarPantallaFin() {
  if (preguntasRespuestas.length === 0) {
    document.getElementById("fin").style.display = "block";
    document.getElementById("game").style.display = "none";
  }
  mostrarPuntos();
}

function mostrarPregunta() {
  console.log("Mostrar pregunta número " + currentQuestion);
  let textoPregunta = document.getElementById("texto-pregunta");
  textoPregunta.innerHTML = preguntasRespuestas[currentQuestion].question;
}

function mostrarRespuestas() {
  let respuestas = document.getElementById("respuestas");

  respuestas.innerHTML = "";

  let opciones = preguntasRespuestas[currentQuestion].options;
  for (let i = 0; i < opciones.length; i++) {
    let input = document.createElement("input");
    let span = document.createElement("span");
    let br = document.createElement("br");
    input.setAttribute("type", "radio");
    input.setAttribute("name", "opcion");
    input.defaultChecked = false;
    input.checked = false;
    input.value = opciones[i];
    span.innerHTML = opciones[i];
    respuestas.append(input);
    respuestas.append(span);
    respuestas.append(br);
  }
}

function mostrarCorreccion() {
  console.log("mostrarCorreccion");
  document.getElementById("game").style.display = "block";
  document.getElementById("check").style.display = "block";
  document.getElementById("pregunta").style.display = "none";
  document.getElementById("respuestas").style.display = "none";
}

function mostrarPuntos() {
  /*   let textoPuntos = document.getElementById("texto-puntos")
  textoPuntos.innerHTML = score + "/" + preguntasTotales */
  debugger
  let textoPuntos = document.getElementById("texto-puntos");
  if (score < 4) {
    textoPuntos.innerHTML =
      "Uf. Qué perecita das <br>" + score + "/" + preguntasTotales + " puntos";
  }
  if (score >= 4 && score < 7) {
    textoPuntos.innerHTML =
      "Baia, baia <br>" + score + "/" + preguntasTotales + " puntos";
  }
  if (score >= 7) {
    textoPuntos.innerHTML =
      "Mis diese <br>" + score + "/" + preguntasTotales + " puntos";
  }
}

function irAlJuego() {
  inicializarArray()  
  mostrarContadorPreguntas();
  currentQuestion = preguntaRandom();
  score = 0;
  console.log("ziortza")
  nivelAvance = valorBarra
  barraAvance()
  console.log("ziortza1")
  preguntasTotales = preguntasRespuestas.length;
  pantallasBloqueadas();
  mostrarPantallaGame();  
  otraPregunta();
}

function preguntaSiguiente() {
  document.getElementById("pregunta").style.display = "block";
  document.getElementById("respuestas").style.display = "block";
  document.getElementById("check").style.display = "none";
  document.getElementById("buttonCheck").style.display = "block";
  currentQuestion = preguntaRandom();
  console.log("pregunta siguiente ");
  mostrarPantallaFin();
  otraPregunta();
}

function otraPregunta() {
  mostrarPregunta();
  mostrarRespuestas();
}

function comprobarCorrecta() {
  let textoCheck = document.getElementById("texto-check");

  let valor = document.querySelector("input[name='opcion']:checked").value;
  //console.log(valor)

  let ok = preguntasRespuestas[currentQuestion].answer;
  if (valor == ok) {
    score++;
    mostrarCorreccion();
    textoCheck.innerHTML = "Efectivy Wonder";
  } else {
    mostrarCorreccion();
    textoCheck.innerHTML = "¡Casi! Pero no";
  }
  document.getElementById("buttonCheck").style.display = "none";
  let preguntasContestadas = preguntasRespuestas.splice(currentQuestion, 1);
  barraAvance();
  mostrarContadorPreguntas();
}

function volverInicio() {
  document.getElementById("start").style.display = "block";
  document.getElementById("fin").style.display = "none";
}

function preguntaRandom() {
  var number = Math.floor(Math.random() * preguntasRespuestas.length);
  /*if (number == -1) {
    number = 0;
  }*/
  return number;
}

function barraAvance() {
  document.getElementsByClassName("barSpan")[0].style.width = nivelAvance + "%";
  nivelAvance = nivelAvance + valorBarra;
}

function mostrarContadorPreguntas() {
  var currentQuestionText = document.getElementById("questionCounter");
  numPreguntas = preguntasRespuestas.length;
  contadorPregunta++;
  currentQuestionText.innerText = 'left: ' + numPreguntas;
}

try {
  module.exports = {
    mostrarPantallaGame,
    mostrarPregunta,
    volverAtras,
    aparecerBotonNext,
    siguientePantalla
  };
} catch { }
